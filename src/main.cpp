/*
 * SD Test for DocodeModem mini V3
 * Copyright (c) 2022 Circuit Desgin,Inc
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
 */

#include <docodemo.h>
#include <SD.h>

DOCODEMO Dm = DOCODEMO();

void error(const char *msg)
{
  SerialDebug.println(msg);
  Dm.BeepVolumeCtrl(3);

  Dm.LedCtrl(GREEN_LED, OFF);
  while (1)
  {
    Dm.LedCtrl(RED_LED, ON);
    delay(500);
    Dm.LedCtrl(RED_LED, OFF);
    delay(500);
  }
}

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  Dm.begin();

  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);

  digitalWrite(SD_PW, HIGH);//Power ON for V3
  pinMode(SD_CS, OUTPUT);

  if (!SD.begin(SD_CS))
  {
    error("SD card begin error");
  }

  File dataFile = SD.open("/datalog.txt", FILE_WRITE);
  if (dataFile)
  {
    dataFile.println("HelloWorld");
    dataFile.flush();
    dataFile.close();
  }
  else
  {
    error("SD card open error");
  }

  dataFile = SD.open("/datalog.txt");
  if (dataFile)
  {
    String str;
    while (dataFile.available())
    {
      str += char(dataFile.read());
    }
    SerialDebug.print(F(" > "));
    SerialDebug.print(str);
    if (str.indexOf("HelloWorld") == -1)
    {
      error("SD card read error");
    }
  }
  else
  {
    error("SD card write error");
  }

  if (!SD.remove("/datalog.txt"))
  {
    error("SD card erase error");
  }

  SerialDebug.println("OK!");

  digitalWrite(SD_PW, LOW); // Power OFF for V3

  while (1)
  {
    Dm.LedCtrl(GREEN_LED, ON);
    Dm.LedCtrl(RED_LED, ON);
    delay(500);
    Dm.LedCtrl(RED_LED, OFF);
    Dm.LedCtrl(GREEN_LED, OFF);
    delay(500);
  }
}

void setup()
{
  SerialDebug.begin(115200);

  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error: scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
}