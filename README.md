# どこでもでむMiniV3向けSDカードサンプルプログラム


こちらを利用させていただいております。<br>
https://github.com/adafruit/SD<br>


旧基板の場合は、digitalWrite(SD_PW, HIGH);やdigitalWrite(SD_PW, LOW);は不要です。<br>

ご注意<br>
SDカードを使う場合にSD_PWをHIGHにしてSDカードに電源をいれるのですが、<br>
その信号がE2PROMのWriteProtectをHIGHにしてしまうので、E2PROMに書き込みができなくなります。<br>
E2PROMに書き込む場合はSD_PWをLOWにしてください。<br>
また省エネのため、SD_PWをLOWにしておいたほうが良いです。
